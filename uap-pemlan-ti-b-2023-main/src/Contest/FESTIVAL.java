package Contest;

class FESTIVAL extends TiketKonser {
    public FESTIVAL(String nama, double harga) {
        super(nama, harga);
    }

    @Override
    public double hitungHarga() {
        return harga;
    }
}