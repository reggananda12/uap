package Contest;

class PemesananTiket {
    private static TiketKonser[] tiketKonser;

    static {
        tiketKonser = new TiketKonser[]{
            new CAT8("CAT 8", 200000),
            new CAT1("CAT 1", 400000),
            new FESTIVAL("FESTIVAL", 5500000),
            new VIP("VIP", 15500000),
            new VVIP("UNLIMITED EXPERIENCE", 21000000)
        };
    }

    public static TiketKonser pilihTiket(int index) {
        return tiketKonser[index];
    }
}