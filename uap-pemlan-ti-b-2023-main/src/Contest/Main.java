/*
 * UAP PEMLAN 2023
 * DURASI: 120 MENIT
 * TEMPAT PENGERJAAN: DARING/LURING
 * =================================================================
 * Semangat mengerjakan UAP teman-teman
 * Jangan lupa berdoa untuk hasil yang terbaik
 */
package Contest;

import java.util.*;
import java.text.SimpleDateFormat;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Selamat datang di pemesanan tiket Coldplay!");

        try {
            System.out.print("Masukkan nama pemesan: ");
            String namaPemesan = scanner.nextLine();

            if (namaPemesan.length() > 10) {
                throw new InvalidInputException("Panjang nama pemesan maksimal 10 karakter");
            }

            System.out.println("Pilih jenis tiket:");
            System.out.println("1.CAT 8");
            System.out.println("2.CAT 1");
            System.out.println("3.FESTIVAL");
            System.out.println("4.VIP");
            System.out.println("5.UNLIMITED EXPERIENCE");
            System.out.println("Masukkan pilihan: ");

            int faktur;
            try {
                faktur = Integer.parseInt(scanner.nextLine());
            } catch (NumberFormatException e) {
                throw new InvalidInputException("Input tidak sesuai! Masukkan pilihan tiket dengan angka.");
            }

            if (faktur < 1 || faktur > 5) {
                throw new InvalidInputException("Input tidak sesuai! Pilih angka 1-5.");
            }

            TiketKonser tiket = PemesananTiket.pilihTiket(faktur - 1);

            String kodeBooking = generateKodeBooking();

            String tanggalPesanan = getCurrentDate();

            System.out.println("---- Detail Pemesanan ----");
            System.out.println("Nama Pemesan: " + namaPemesan);
            System.out.println("Kode Booking: " + kodeBooking);
            System.out.println("Tanggal Pesanan: " + tanggalPesanan);
            System.out.println("Tiket yang dipesan: " + tiket.getNama());
            System.out.println("Total harga: " + tiket.hitungHarga() + " USD");
        } catch (InvalidInputException e) {
            System.out.println("Terjadi kesalahan: " + e.getpesan());
        } catch (Exception e) {
            System.out.println("Terjadi kesalahan: " + ((InvalidInputException) e).getpesan());
        } finally {
            scanner.close();
        }
    }
     /*
     * Jangan ubah isi method dibawah ini, nama method boleh diubah
     * Method ini dipanggil untuk mendapatkan kode pesanan atau kode booking
     * Panggil method ini untuk kelengkapan mencetak output nota pesanan
     */
    public static String generateKodeBooking() {
        StringBuilder sb = new StringBuilder();
        String characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        int length = 8;

        for (int i = 0; i < length; i++) {
            int index = (int) (Math.random() * characters.length());
            sb.append(characters.charAt(index));
        }

        return sb.toString();
    }
     /*
     * Jangan ubah isi method dibawah ini, nama method boleh diubah
     * Method ini dipanggil untuk mendapatkan waktu terkini
     * Panggil method ini untuk kelengkapan mencetak output nota pesanan
     */
    public static String getCurrentDate() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        Date currentDate = new Date();
        return dateFormat.format(currentDate);
    }
}