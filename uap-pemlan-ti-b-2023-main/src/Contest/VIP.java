package Contest;

class VIP extends TiketKonser {
    public VIP(String nama, double harga) {
        super(nama, harga);
    }

    @Override
    public double hitungHarga() {
        return harga;
    }
}