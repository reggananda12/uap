package Contest;

class CAT1 extends TiketKonser {
    public CAT1(String nama, double harga) {
        super(nama, harga);
    }

    @Override
    public double hitungHarga() {
        return harga;
    }
}