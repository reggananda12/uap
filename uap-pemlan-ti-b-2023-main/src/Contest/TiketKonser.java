package Contest;

abstract class TiketKonser implements HargaTiket {
    protected String nama;
    protected double harga;

    public TiketKonser(String nama, double harga) {
        this.nama = nama;
        this.harga = harga;
    }

    public String getNama() {
        return nama;
    }

    public abstract double hitungHarga();
}