package Contest;

class VVIP extends TiketKonser {
    public VVIP(String nama, double harga) {
        super(nama, harga);
    }

    @Override
    public double hitungHarga() {
        return harga;
    }
}